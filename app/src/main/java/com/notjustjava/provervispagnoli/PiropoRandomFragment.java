package com.notjustjava.provervispagnoli;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.notjustjava.provervispagnoli.db.ProverbsDBHelper;
import com.notjustjava.provervispagnoli.util.ProverbiSpagnoliConstants;

public class PiropoRandomFragment extends Fragment {

    private long proverbId;

    public PiropoRandomFragment() {
    }

    public static PiropoRandomFragment newInstance() {
        PiropoRandomFragment fragment = new PiropoRandomFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_random_piropo, container, false);

        newPiropoRandomProverb(view.getRootView());
        refreshFavoriteButton(view.getRootView());

        view.findViewById(R.id.button_random_proverb_piropo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newPiropoRandomProverb(v.getRootView());
                refreshFavoriteButton(v.getRootView());
            }
        });

        view.findViewById(R.id.favorite_random_button_piropo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProverbsDBHelper dbHelper = ProverbsDBHelper.getInstance(getActivity());
                boolean isFavorite = dbHelper.isFavoriteProverb(proverbId);
                if (isFavorite) {
                    dbHelper.deleteFavorite(proverbId);
                } else {
                    dbHelper.insertFavorite(proverbId);
                }

                refreshFavoriteButton(v.getRootView());

            }
        });

        view.findViewById(R.id.share_random_proverb_piropo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("Compartiendo un proverbio");
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                ProverbsDBHelper dbHelper = ProverbsDBHelper.getInstance(getActivity());
                Cursor cursor = dbHelper.getProverbById(proverbId);

                sharingIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_proverb_message, cursor.getString(1)));

                startActivity(Intent.createChooser(sharingIntent, getString(R.string.condividere_properb)));
            }
        });

        AdView mAdView = (AdView) view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        return view;

    }

    private void newPiropoRandomProverb(View view) {

        ProverbsDBHelper dbHelper = ProverbsDBHelper.getInstance(getActivity());

        Cursor cursor = dbHelper.getPiropoRandomProverb();

        proverbId = cursor.getLong(0);
        System.out.println("Id proverbio: " + proverbId);

        TextView proverb = (TextView) view.findViewById(R.id.proverb_piropo);
        TextView literal = (TextView) view.findViewById(R.id.literal_translation_piropo);
        TextView translation = (TextView) view.findViewById(R.id.translation_piropo);

        String test = cursor.getString(1);
         test = cursor.getString(3);
         test = cursor.getString(2);

        proverb.setText(cursor.getString(1));
        literal.setText(cursor.getString(3));
        translation.setText(cursor.getString(2));
        cursor.close();

    }

    private void refreshFavoriteButton(View v) {

        ProverbsDBHelper dbHelper = ProverbsDBHelper.getInstance(getActivity());

        boolean isFavorite = dbHelper.isFavoriteProverb(proverbId);

        ImageButton buttonFavorite = (ImageButton)v.findViewById(R.id.favorite_random_button_piropo);

        if (isFavorite) {
            buttonFavorite.setImageResource(R.drawable.ic_favorite_white_48dp);
        } else {
            buttonFavorite.setImageResource(R.drawable.ic_favorite_border_white_48dp);
        }

    }


}
