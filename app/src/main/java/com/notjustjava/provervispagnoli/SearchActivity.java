package com.notjustjava.provervispagnoli;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.notjustjava.provervispagnoli.adapter.CategoryListItemAdapter;
import com.notjustjava.provervispagnoli.db.ProverbsDBHelper;
import com.notjustjava.provervispagnoli.dto.Row;
import com.notjustjava.provervispagnoli.util.ProverbiSpagnoliConstants;
import com.notjustjava.provervispagnoli.util.ShareUtils;

import java.util.List;

public class SearchActivity extends AppCompatActivity {

    private CategoryListItemAdapter listItemAdapter;
    private ListView listView;
    private ShareActionProvider shareActionProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Intent intent = getIntent();

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            List<Row> list = ProverbsDBHelper.getInstance(this).searchProverbList(query);
            listView = (ListView) findViewById(android.R.id.list);
            listItemAdapter = new CategoryListItemAdapter(this, android.R.id.list, list);
            listView.setAdapter(listItemAdapter);

            listView.setEmptyView(findViewById(android.R.id.empty));
            ((TextView)findViewById(R.id.no_result_found)).setText(getString(R.string.search_no_result_found, query));
        }

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        //mando senial a GoogleAnalytics
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        Tracker tracker = analytics.newTracker(ProverbiSpagnoliConstants.GOOGLE_ANALITICS_CODE);

        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("Busqueda")
                .setAction("click")
                .setLabel("Busqueda")
                .build());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        //Set up color of actionbar
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#BF360C")));

        //programo el boton de buscar
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        //activo el boton de compartir
        MenuItem shareMenuItem = menu.findItem(R.id.share_app);
        shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareMenuItem);
        shareActionProvider.setShareIntent(ShareUtils.getShareAppIntent());


        return true;
    }

}
