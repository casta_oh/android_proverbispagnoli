package com.notjustjava.provervispagnoli;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.notjustjava.provervispagnoli.db.ProverbsDBHelper;

public class InsultoRandomFragment extends Fragment implements View.OnClickListener{

    private long proverbId;

    public InsultoRandomFragment() {

    }

    public static InsultoRandomFragment newInstance() {
        InsultoRandomFragment fragment = new InsultoRandomFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_random_insulto, container, false);


        newInsultoRandomProverb(view.getRootView());
        refreshInsultoButton(view.getRootView());

        view.findViewById(R.id.button_random_proverb_insulto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newInsultoRandomProverb(v.getRootView());
                refreshInsultoButton(v.getRootView());
            }
        });

        view.findViewById(R.id.favorite_random_button_insulto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProverbsDBHelper dbHelper = ProverbsDBHelper.getInstance(getActivity());
                boolean isFavorite = dbHelper.isFavoriteProverb(proverbId);
                if (isFavorite) {
                    dbHelper.deleteFavorite(proverbId);
                } else {
                    dbHelper.insertFavorite(proverbId);
                }

                refreshInsultoButton(v.getRootView());

            }
        });

        view.findViewById(R.id.share_random_proverb_insulto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("Compartiendo un proverbio");
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                ProverbsDBHelper dbHelper = ProverbsDBHelper.getInstance(getActivity());
                Cursor cursor = dbHelper.getProverbById(proverbId);

                sharingIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_proverb_message, cursor.getString(1)));

                startActivity(Intent.createChooser(sharingIntent, getString(R.string.condividere_properb)));
            }
        });

        AdView mAdView = (AdView) view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        return view;

    }

    private void newInsultoRandomProverb(View view) {

        ProverbsDBHelper dbHelper = ProverbsDBHelper.getInstance(getActivity());

        Cursor cursor = dbHelper.getInsultoRandomProverb();

        proverbId = cursor.getLong(0);
        System.out.println("Id proverbio: " + proverbId);

        TextView proverb = (TextView) view.findViewById(R.id.proverb_insulto);
        TextView literal = (TextView) view.findViewById(R.id.literal_translation_insulto);
        TextView translation = (TextView) view.findViewById(R.id.translation_insulto);

        proverb.setText(cursor.getString(1));
        literal.setText(cursor.getString(3));
        translation.setText(cursor.getString(2));
        cursor.close();

    }

    @Override
    public void onClick(View v) {

        newInsultoRandomProverb(v.getRootView());

    }

    private void refreshInsultoButton(View v) {

        ProverbsDBHelper dbHelper = ProverbsDBHelper.getInstance(getActivity());

        boolean isFavorite = dbHelper.isFavoriteProverb(proverbId);

        ImageButton buttonFavorite = (ImageButton)v.findViewById(R.id.favorite_random_button_insulto);

        if (isFavorite) {
            buttonFavorite.setImageResource(R.drawable.ic_favorite_white_48dp);
        } else {
            buttonFavorite.setImageResource(R.drawable.ic_favorite_border_white_48dp);
        }

    }
}
