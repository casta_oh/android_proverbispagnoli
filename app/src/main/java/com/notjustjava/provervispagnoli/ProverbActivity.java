package com.notjustjava.provervispagnoli;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.notjustjava.provervispagnoli.db.ProverbsDBHelper;


public class ProverbActivity extends AppCompatActivity {

    Long proverbId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proverb);

        newRandomProverb();


        //Set up color of actionbar
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#BF360C")));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_proverb, menu);

        return true;
    }

    public Intent getShareProverbIntent() {

        System.out.println("Compartiendo un proverbio");
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        //sharingIntent.putExtra(Intent.EXTRA_TEXT, cursor.getString(2));
        //sharingIntent.putExtra(Intent.EXTRA_SUBJECT, cursor.getString(1));

        return sharingIntent;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {


        return true;
    }

    private void newRandomProverb() {

        ProverbsDBHelper dbHelper = ProverbsDBHelper.getInstance(this);

        Cursor cursor = dbHelper.getRandomProverb();

        proverbId = cursor.getLong(0);
        System.out.println("Id proverbio: " + proverbId);

        TextView proverb        = (TextView) this.findViewById(R.id.proverb);
        TextView literal        = (TextView) this.findViewById(R.id.literal_translation);
        TextView translation    = (TextView) this.findViewById(R.id.translation);

        proverb.setText(cursor.getString(1));
        literal.setText(cursor.getString(3));
        translation.setText(cursor.getString(2));
        cursor.close();

    }

    private void refreshFavoriteButton() {

        ProverbsDBHelper dbHelper = ProverbsDBHelper.getInstance(this);
        boolean isFavorite = dbHelper.isFavoriteProverb(proverbId);
        ImageButton buttonFavorite = (ImageButton)this.findViewById(R.id.favorite_random_button);
        if (isFavorite) {
            buttonFavorite.setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
        } else {
            buttonFavorite.setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
        }

    }

}
