package com.notjustjava.provervispagnoli;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.notjustjava.provervispagnoli.adapter.CategoryListItemAdapter;
import com.notjustjava.provervispagnoli.adapter.FavoritesListItemAdapter;
import com.notjustjava.provervispagnoli.db.ProverbsDBHelper;
import com.notjustjava.provervispagnoli.dto.Row;

import java.util.List;

public class ListItemFragment extends ListFragment {

    private int categoryId;
    private final static String CATEGORY_ID = "CATEGORY_ID";
    private CategoryListItemAdapter listItemAdapter;
    private ListView listView;

    public ListItemFragment() {
    }

    public static ListItemFragment newInstance(int categoryId) {
        ListItemFragment fragment = new ListItemFragment();
        Bundle args = new Bundle();
        args.putInt(CATEGORY_ID, categoryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        List<Row> list = ProverbsDBHelper.getInstance(getActivity()).getListProverbsAndFavoriteByCategory(categoryId);
        listItemAdapter = new CategoryListItemAdapter(getActivity(), android.R.id.list, list);
        listView.setAdapter(listItemAdapter);

        AdView mAdView = (AdView) getActivity().findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            categoryId = getArguments().getInt(CATEGORY_ID);
        } else {
            categoryId = 1;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_list_favorites, container, false);
        listView = (ListView) rootView.findViewById(android.R.id.list);
        return rootView;

    }



}
