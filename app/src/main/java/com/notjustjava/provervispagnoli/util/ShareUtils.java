package com.notjustjava.provervispagnoli.util;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import com.notjustjava.provervispagnoli.R;
import com.notjustjava.provervispagnoli.db.ProverbsDBHelper;

public class ShareUtils {

    public static Intent getShareAppIntent() {

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");

        String sAux = "\nTi consiglio questa app per conoscere i proverbi spagnoli più usati\n\n";
        sAux = sAux + "https://play.google.com/store/apps/details?id=com.notjustjava.provervispagnoli \n\n";

        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, sAux);

        return sharingIntent;

    }

}
