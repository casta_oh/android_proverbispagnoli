package com.notjustjava.provervispagnoli.util;


import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class ProverbiSpagnoliUtils {

    public static float dipToPixels(Context context, float dipValue) {
        System.out.println("Obteniendo los pixels equivalentes a partir de la densidad");
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();

        float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
        System.out.println("Pixels obtenidos: " + pixels);
        return pixels;
    }


}
