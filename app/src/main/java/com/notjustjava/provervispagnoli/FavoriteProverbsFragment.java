package com.notjustjava.provervispagnoli;


import android.app.LoaderManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.notjustjava.provervispagnoli.adapter.FavoritesListItemAdapter;
import com.notjustjava.provervispagnoli.adapter.ListItemAdapter;
import com.notjustjava.provervispagnoli.db.ProverbsDBHelper;
import com.notjustjava.provervispagnoli.dto.Row;
import com.notjustjava.provervispagnoli.util.ProverbiSpagnoliConstants;

import java.util.List;

public class FavoriteProverbsFragment extends ListFragment {

    private ListView listView;
    FavoritesListItemAdapter liAdapter;


    public static FavoriteProverbsFragment newInstance() {
        FavoriteProverbsFragment fragment = new FavoriteProverbsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        List<Row> list = ProverbsDBHelper.getInstance(getActivity()).getListFavoritesProverbs();
        liAdapter = new FavoritesListItemAdapter(getActivity(), android.R.id.list, list);
        listView.setAdapter(liAdapter);

        listView.setEmptyView(getActivity().findViewById(android.R.id.empty));
        ((TextView)getActivity().findViewById(R.id.no_result_found)).setText(getString(R.string.no_favorites_proverbs));

        AdView mAdView = (AdView) getActivity().findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_list_favorites, container, false);

        listView = (ListView) rootView.findViewById(android.R.id.list);

        return rootView;


    }

}
