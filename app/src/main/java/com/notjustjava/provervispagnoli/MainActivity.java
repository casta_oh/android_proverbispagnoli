package com.notjustjava.provervispagnoli;

import android.app.SearchManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.ShareActionProvider;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.notjustjava.provervispagnoli.sliding.SlidingTabsBasicFragment;
import com.notjustjava.provervispagnoli.util.ProverbiSpagnoliConstants;
import com.notjustjava.provervispagnoli.util.ProverbiSpagnoliUtils;
import com.notjustjava.provervispagnoli.util.ShareUtils;

public class MainActivity extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private ShareActionProvider shareActionProvider;

    private CharSequence mTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

    }

    private void registerPageGoogleAnalytics(String label) {

        //mando senial a GoogleAnalytics
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        Tracker tracker = analytics.newTracker(ProverbiSpagnoliConstants.GOOGLE_ANALITICS_CODE);

        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("Filtrado")
                .setAction("click")
                .setLabel(label)
                .build());

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        getSupportActionBar().setElevation(ProverbiSpagnoliUtils.dipToPixels(this, 8));



        switch (position) {
            case 0:
                getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
                registerPageGoogleAnalytics(getResources().getString(R.string.app_name));
                getSupportActionBar().setElevation(ProverbiSpagnoliUtils.dipToPixels(this, 0));
                SlidingTabsBasicFragment fragment = new SlidingTabsBasicFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment).commit();
                break;
            case 1:
                getSupportActionBar().setTitle(getResources().getString(R.string.title_section8));
                registerPageGoogleAnalytics(getResources().getString(R.string.title_section8));
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ListItemFragment.newInstance(8))
                        .commit();
                break;
            case 2:
                getSupportActionBar().setTitle(getResources().getString(R.string.title_section1));
                registerPageGoogleAnalytics(getResources().getString(R.string.title_section1));
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ListItemFragment.newInstance(1))
                        .commit();

                break;
            case 3:
                getSupportActionBar().setTitle(getResources().getString(R.string.title_section2));
                registerPageGoogleAnalytics(getResources().getString(R.string.title_section2));
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ListItemFragment.newInstance(2))
                        .commit();
                break;
            case 4:
                getSupportActionBar().setTitle(getResources().getString(R.string.title_section3));
                registerPageGoogleAnalytics(getResources().getString(R.string.title_section3));
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ListItemFragment.newInstance(3))
                        .commit();
                break;
            case 5:
                getSupportActionBar().setTitle(getResources().getString(R.string.title_section4));
                registerPageGoogleAnalytics(getResources().getString(R.string.title_section4));
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ListItemFragment.newInstance(4))
                        .commit();
                break;
            //case 6:
            //    getSupportActionBar().setTitle(getResources().getString(R.string.title_section5));
            //    fragmentManager.beginTransaction()
            //            .replace(R.id.container, ListItemFragment.newInstance(5))
            //            .commit();
            //    break;
            case 6:
                getSupportActionBar().setTitle(getResources().getString(R.string.title_section6));
                registerPageGoogleAnalytics(getResources().getString(R.string.title_section6));
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ListItemFragment.newInstance(6))
                        .commit();
                break;
            case 7:
                getSupportActionBar().setTitle(getResources().getString(R.string.title_section7));
                registerPageGoogleAnalytics(getResources().getString(R.string.title_section7));
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ListItemFragment.newInstance(7))
                        .commit();
                break;
            case 8:
                getSupportActionBar().setTitle(getResources().getString(R.string.title_section9));
                registerPageGoogleAnalytics(getResources().getString(R.string.title_section9));
                fragmentManager.beginTransaction()
                        .replace(R.id.container, FavoriteProverbsFragment.newInstance())
                        .commit();
                break;
            case 9:
                getSupportActionBar().setTitle(getResources().getString(R.string.title_section10));
                registerPageGoogleAnalytics(getResources().getString(R.string.title_section10));
                fragmentManager.beginTransaction()
                        .replace(R.id.container, VivereAMadridFragment.newInstance())
                        .commit();
                break;
            default:
                getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
                registerPageGoogleAnalytics(getResources().getString(R.string.app_name));
                getSupportActionBar().setElevation(0);
                SlidingTabsBasicFragment fragment2 = new SlidingTabsBasicFragment();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment2).commit();
                break;
        }

    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section8);
                break;
            case 3:
                mTitle = getString(R.string.title_section2);
                break;
            case 4:
                mTitle = getString(R.string.title_section3);
                break;
            case 5:
                mTitle = getString(R.string.title_section4);
                break;
            //case 6:
            //    mTitle = getString(R.string.title_section5);
            //    break;
            case 6:
                mTitle = getString(R.string.title_section6);
                break;
            case 7:
                mTitle = getString(R.string.title_section7);
                break;
            case 8:
                mTitle = getString(R.string.title_section9);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (!mNavigationDrawerFragment.isDrawerOpen()) {

            getMenuInflater().inflate(R.menu.main, menu);
            //activo el boton de compartir
            MenuItem shareMenuItem = menu.findItem(R.id.share_app);
            shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareMenuItem);
            shareActionProvider.setShareIntent(ShareUtils.getShareAppIntent());

            //programo el boton de buscar
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

            // Assumes current activity is the searchable activity
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

            //Set up color of actionbar
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#BF360C")));
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
