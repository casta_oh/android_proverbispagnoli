package com.notjustjava.provervispagnoli.dto;


public class Row {

    private Long proverbId;
    private String proverb;
    private String literalTranslation;
    private String translation;
    private boolean favorite;
    private Integer categoryId;

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public Long getProverbId() {
        return proverbId;
    }

    public void setProverbId(Long proverbId) {
        this.proverbId = proverbId;
    }

    public String getProverb() {
        return proverb;
    }

    public void setProverb(String proverb) {
        this.proverb = proverb;
    }

    public String getLiteralTranslation() {
        return literalTranslation;
    }

    public void setLiteralTranslation(String literalTranslation) {
        this.literalTranslation = literalTranslation;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

}
