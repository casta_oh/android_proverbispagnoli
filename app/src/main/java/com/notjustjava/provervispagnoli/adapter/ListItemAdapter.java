package com.notjustjava.provervispagnoli.adapter;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.opengl.Visibility;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.notjustjava.provervispagnoli.R;
import com.notjustjava.provervispagnoli.db.ProverbsDBHelper;
import com.notjustjava.provervispagnoli.dto.Row;
import java.util.List;

public abstract class ListItemAdapter extends ArrayAdapter<Row> {

    public ListItemAdapter(Context context, int resource, List<Row> items) {
        super(context, resource, items);
    }

    protected void setBackGroundColor(Integer categoryId, View v) {

        if (categoryId == null) {
            v.findViewById(R.id.test).setBackgroundColor(v.getResources().getColor(R.color.color_random));
        } else {
            switch (categoryId) {
                case 0:
                    v.findViewById(R.id.test).setBackgroundColor(v.getResources().getColor(R.color.color_piropo));
                    break;
                case 1: //animali
                    v.findViewById(R.id.test).setBackgroundColor(v.getResources().getColor(R.color.categoria_animali));
                    break;
                case 2: //complimenti
                    v.findViewById(R.id.test).setBackgroundColor(v.getResources().getColor(R.color.categoria_complimenti));
                    break;
                case 3: //offese
                    v.findViewById(R.id.test).setBackgroundColor(v.getResources().getColor(R.color.categoria_offese));
                    break;
                case 4: //cibo
                    v.findViewById(R.id.test).setBackgroundColor(v.getResources().getColor(R.color.categoria_cibo));
                    break;
                case 5: //natura
                    v.findViewById(R.id.test).setBackgroundColor(v.getResources().getColor(R.color.categoria_cibo));
                    break;
                case 6: //taurini
                    v.findViewById(R.id.test).setBackgroundColor(v.getResources().getColor(R.color.categoria_taurini));
                    break;
                case 7: //tradizionali
                    v.findViewById(R.id.test).setBackgroundColor(v.getResources().getColor(R.color.categoria_tradizionali));
                    break;
                case 8: //amore
                    v.findViewById(R.id.test).setBackgroundColor(v.getResources().getColor(R.color.categoria_amore));
                    break;
                case 9: //amore
                    v.findViewById(R.id.test).setBackgroundColor(v.getResources().getColor(R.color.categoria_complimenti));
                    break;
                default:
                    v.findViewById(R.id.test).setBackgroundColor(v.getResources().getColor(R.color.color_random));
                    break;
            }
        }

    }


    protected View.OnClickListener shareListener = new View.OnClickListener()
    {

        @Override
        public void onClick(View v)
        {

            Row row = (Row)v.getTag();

            System.out.println("Compartiendo un proverbio");
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");

            sharingIntent.putExtra(Intent.EXTRA_TEXT, v.getResources().getString(R.string.share_proverb_message, row.getProverb()));

            v.getContext().startActivity(Intent.createChooser(sharingIntent,
                    v.getContext().getString(R.string.condividere_properb)));

        }
    };


}
