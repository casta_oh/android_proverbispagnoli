package com.notjustjava.provervispagnoli.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.notjustjava.provervispagnoli.InsultoRandomFragment;

public class TabsPagerAdapter extends FragmentPagerAdapter {

    public TabsPagerAdapter(FragmentManager fm) {

        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        return InsultoRandomFragment.newInstance();
    }

    @Override
    public int getCount() {

        return 3;
    }
}
