package com.notjustjava.provervispagnoli.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.notjustjava.provervispagnoli.R;
import com.notjustjava.provervispagnoli.db.ProverbsDBHelper;
import com.notjustjava.provervispagnoli.dto.Row;

import java.util.List;

public class FavoritesListItemAdapter extends ListItemAdapter {

    public FavoritesListItemAdapter(Context context, int resource, List<Row> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.fragment_list_proverbs, null);
        }

        Row row = getItem(position);
        setBackGroundColor(row.getCategoryId(), v);

        if (row != null) {

            TextView tt1 = (TextView) v.findViewById(R.id.proverb);
            TextView tt2 = (TextView) v.findViewById(R.id.literal_translation);
            TextView tt3 = (TextView) v.findViewById(R.id.translation);

            tt1.setText(row.getProverb());
            tt2.setText(row.getLiteralTranslation());
            tt3.setText(row.getTranslation());

            v.findViewById(R.id.share_proverb).setOnClickListener(shareListener);
            v.findViewById(R.id.share_proverb).setTag(row);

            v.findViewById(R.id.favorite_proverb).setOnClickListener(deleteFavoriteListener);
            v.findViewById(R.id.favorite_proverb).setTag(row);

        }

        return v;

    }

    protected View.OnClickListener deleteFavoriteListener = new View.OnClickListener()
    {

        @Override
        public void onClick(View v)
        {

            Row row = (Row)v.getTag();

            ProverbsDBHelper dbHelper = ProverbsDBHelper.getInstance(v.getContext());
            System.out.println("Borrando un favorito un proverbio");
            dbHelper.deleteFavorite(row.getProverbId());

            remove(row);
            notifyDataSetChanged();


        }
    };

}
